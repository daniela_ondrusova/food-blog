<?php
// Include the connection file
require_once 'connection.php';

// Handle like button submission
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['like'])) {
    handleLike($conn);
}

// Function to handle like button submission
function handleLike($conn) 
{
    $id = $_POST['like'];
    $sql = "UPDATE posts SET likes = likes + 1 WHERE id = ?";
    
    try {
        $stmt = $conn->prepare($sql);
        $stmt->execute([$id]);
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
    
    // Redirect to the index page after processing the like
    header("Location: index.php");
    exit();
}

// Function to fetch popular chefs
function getPopularChefs($conn) 
{
    $query = "SELECT auteurs.naam, SUM(posts.likes) AS 'totaal_likes'
              FROM posts
              INNER JOIN auteurs ON posts.auteur_id = auteurs.id
              GROUP BY auteur_id
              HAVING SUM(posts.likes) >= 10";

    return fetchData($conn, $query);
}

// Function to fetch posts with authors
function getPostsAndAuthors($conn) 
{
    $query = 'SELECT *, auteurs.naam 
              FROM auteurs 
              INNER JOIN posts ON posts.auteur_id = auteurs.id
              ORDER BY likes DESC';

    $postsAndAuthors = fetchData($conn, $query);

    foreach ($postsAndAuthors as &$post) {
        $post['tags'] = getTagsForPost($conn, $post['id']);
    }

    return $postsAndAuthors;
}

// Function to fetch tags for a specific post
function getTagsForPost($conn, $postId)
{
    $tagsQuery = "SELECT tags.titel
                  FROM tags
                  INNER JOIN posts_tags ON tags.id = posts_tags.tag_id
                  WHERE posts_tags.post_id = :postId";

    $tagsStmt = $conn->prepare($tagsQuery);
    $tagsStmt->bindParam(':postId', $postId, PDO::PARAM_INT);
    $tagsStmt->execute();
    return $tagsStmt->fetchAll(PDO::FETCH_ASSOC);
}

// Function to fetch data from the database
function fetchData($conn, $query) 
{
    try {
        $stmt = $conn->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
        return [];
    }
}
?>

<!DOCTYPE html>
<html lang="nl">
<head>
    <title>Foodblog</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <div id="header">
            <h1>Foodblog</h1>
            <span class="right">
                <p><a href="new_post.php">Nieuwe post</a></p>
            </span>
        </div>
        <div>
            <h3>Populaire chefs</h3>
            <ul>
                <?php
                $popularChefs = getPopularChefs($conn);
                foreach ($popularChefs as $row) {
                    ?>
                    <li><?php echo $row['naam'] ?></li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <div class="posts-container">
            <?php
            $postsAndAuthors = getPostsAndAuthors($conn);
            foreach ($postsAndAuthors as $row) {
                ?>
                <div class="post">
                    <div class="header">
                        <h2><?php echo $row['titel'] ?></h2>
                        <img src="<?php echo $row['img_url'] ?>" alt="<?php echo $row['titel'] ?>">
                    </div>
                    <span class="details">Geschreven op <?php echo $row['datum'] ?> door <b><?php echo $row['naam'] ?></b>
                        <span class="right">
                            <form action="index.php" method="post">
                                <button type="submit" value="<?php echo $row['id']; ?>" name="like">
                                    <?php echo $row['likes']; ?> likes
                                </button>
                            </form>
                        </span>
                        <div>
                        <?php
                        $postId = $row['id'];
                        $tags = getTagsForPost($conn, $postId);
                            echo "<p>Tags: ";
                        foreach ($tags as $tag) {
                                $tagTitle = $tag['titel'];
                                $tagLink = "lookup.php?tag=$tagTitle";
                                echo "<a href='$tagLink'>$tagTitle</a> ";
                        }
                            echo "</p>"; 
                        ?>
                        </div>
                    </span>
                    <p><?php echo $row['inhoud'] ?></p>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</body>
</html>